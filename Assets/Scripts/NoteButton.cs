using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum NoteButtonType { None, Left, Mid, Right }

public class NoteButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    #region Variables
    public NoteButtonType noteButtonType = NoteButtonType.None;
    public bool interactable;
    private bool isPressed;
    public Graphic targetGraphic;
    public ColorBlock colors;
    #endregion

    #region Unity Methods

    private void Start()
    {
        StartColorTween(colors.normalColor, true);
    }

    private void OnEnable()
    {
        this.AddObserver(OnVirtualKeyDown, ReMessageCenter.NOTE_VBUTTON_PRESS_DOWN);
        this.AddObserver(OnVirtualKeyUp, ReMessageCenter.NOTE_VBUTTON_PRESS_UP);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnVirtualKeyDown, ReMessageCenter.NOTE_VBUTTON_PRESS_DOWN);
        this.RemoveObserver(OnVirtualKeyUp, ReMessageCenter.NOTE_VBUTTON_PRESS_UP);
    }

#if UNITY_EDITOR

    private void OnValidate()
    {
        StartColorTween(colors.normalColor, true);
    }

#endif

    #endregion Unity Methods

    #region IPointer

    public void OnPointerDown(PointerEventData eventData)
    {
        OnVirtualKeyDown(this, noteButtonType);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnVirtualKeyUp(this, noteButtonType);
    }

    #endregion IPointer

    #region Virtual Key Receiver

    private void OnVirtualKeyDown(object sender, object data)
    {
        if (interactable)
            if ((NoteButtonType)data == noteButtonType)
            {
                this.PostNotification(ReMessageCenter.NOTE_TRIGGERED, noteButtonType);
                isPressed = true;
                StartColorTween(colors.pressedColor, false);
            }
    }

    private void OnVirtualKeyUp(object sender, object data)
    {
        if ((NoteButtonType)data == noteButtonType)
        {
            if (isPressed)
            {
                isPressed = false;
                StartColorTween(colors.normalColor, false);
            }
        }
    }

    #endregion Virtual Key Receiver

    #region Image Color

    private void StartColorTween(Color targetColor, bool instant)
    {
        if (targetGraphic == null)
            return;

        targetGraphic.CrossFadeColor(targetColor, instant ? 0f : colors.fadeDuration, true, true);
    }

    #endregion Image Color

}