using MEC;
using SimpleFileBrowser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region General Variables

    #region Show In Inspector

    [Header("Buttons")]
    [SerializeField] private Button openSongButton;

    [SerializeField] private Button startSongButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private TMP_Text startSongButtonText;

    [Header("Text")]
    [SerializeField] private TMP_Text timeStamp;

    [SerializeField] private TMP_Text timeDebugStamp;
    [SerializeField] private TMP_Text normalHit;
    [SerializeField] private TMP_Text perfectHit;
    [SerializeField] private TMP_Text missHit;

    [Header("Other")]
    [SerializeField] private AudioSource musicSource;

    [SerializeField] private NAudioImporter audioImporter;
    [SerializeField] private Transform[] spawnPoints;

    #endregion Show In Inspector

    #region Privates

    private TimeSpan tempTimeSpan;
    private string musicDataPath = "";
    private string musicName = "";
    private string musicPath = "";
    private double musicLenght;

    private int miss = 0;
    private int hit = 0;
    private int perfect = 0;

    #endregion Privates

    #endregion General Variables

    #region Unity Methods

    private void Start()
    {
        openSongButton.onClick.AddListener(OpenSong);
        startSongButton.onClick.AddListener(StartGame);
        exitButton.onClick.AddListener(ExitGame);
    }

    private void OnEnable()
    {
        audioImporter.Loaded += OnAudioLoaded;
        this.AddObserver(OnNoteToPool, ReMessageCenter.NOTE_TO_POOL);
        this.AddObserver(OnHitScore, ReMessageCenter.NOTE_SCORE);
    }

    private void OnDisable()
    {
        audioImporter.Loaded += OnAudioLoaded;
        gameRunning = false;
        this.RemoveObserver(OnNoteToPool, ReMessageCenter.NOTE_TO_POOL);
        this.RemoveObserver(OnHitScore, ReMessageCenter.NOTE_SCORE);
    }

    private void Update()
    {
        if (gameRunning)
            InputCheck();

    }

    #endregion Unity Methods

    #region Scoring

    private void OnHitScore(object sender, object data)
    {
        int hitType = (int)data;

        if (hitType == 0)
        {
            miss++;
        }
        else if (hitType == 1)
        {
            hit++;
        }
        else
        {
            perfect++;
        }

        UpdateHit();
    }

    private void UpdateHit()
    {
        missHit.text = "x" + miss;
        normalHit.text = "x" + hit;
        perfectHit.text = "x" + perfect;
    }

    private void ResetHit()
    {
        miss = 0;
        hit = 0;
        perfect = 0;
        UpdateHit();
    }

    #endregion Scoring

    #region Load Song Data

    #region Select Load Path

    private void OpenSong()
    {
        openSongButton.interactable = false;
        startSongButton.interactable = false;

        FileBrowser.SetDefaultFilter(".mus");
        FileBrowser.SetFilters(false, new string[] { ".mus" });

        StartCoroutine(ShowLoadDialogCoroutine());
    }

    private IEnumerator ShowLoadDialogCoroutine()
    {
        // Show a load file dialog and wait for a response from user
        // Load file/folder: both, Allow multiple selection: true
        // Initial path: default (Documents), Initial filename: empty
        // Title: "Load File", Submit button text: "Load"
        yield return FileBrowser.WaitForLoadDialog(false, false, Application.dataPath, "Load Music Data", "Load");

        if (FileBrowser.Success)
        {
            this.PostNotification(ReMessageCenter.CONSOLE_ADD_LOG, "Opening File At " + FileBrowserHelpers.GetDirectoryName(FileBrowser.Result[0]));
            musicDataPath = FileBrowser.Result[0];
            musicPath = FileBrowserHelpers.GetDirectoryName(musicDataPath);
            LoadMusicData(FileBrowser.Result[0]);
        }
        else
        {
            startSongButton.interactable = musicDataPath != "";
            openSongButton.interactable = true;
        }
    }

    #endregion Select Load Path

    #region Parse Music Data

    private bool readHeader = false;

    private void LoadMusicData(string path)
    {
        this.PostNotification(ReMessageCenter.CONSOLE_ADD_LOG, "Reading Music Data");

        readHeader = false;
        noteDatas.Clear();
        if (path.Length != 0)
            CVSReader.LoadFromFile(path, DialogDataProcessor, DialogDataProcessComplete);
        else
        {
            this.PostNotification(ReMessageCenter.CONSOLE_ADD_LOG, "Invalid " + path);
            openSongButton.interactable = true;
        }
    }

    private void DialogDataProcessor(int line_index, List<string> line)
    {
        this.PostNotification(ReMessageCenter.CONSOLE_ADD_LOG, "Processing " + line[0]) ;
       // Debug.Log(string.Format("{0} => {1}", line_index, line[0]));

        if (!readHeader)
        {
            if (line[0] != "")
            {
                musicName = line[0];
                musicPath = Path.Combine(musicPath, line[1].Trim());
            }
            readHeader = true;
            return;
        }

       //Debug.Log(string.Format("\n==> Line {0}, {1} column(s)", line_index, line.Count));
       // print(line[0]);
       //print(line[0].Split('#').Length);

        if (line[0] == "" || String.IsNullOrEmpty(line[0]) || line[0].Split('#').Length > 1)
            return;

        NoteData tempNoteData = new NoteData();
        tempNoteData.timeStamp = TimeSpan.ParseExact(line[0], @"m\:s\:ff", System.Globalization.CultureInfo.InvariantCulture).TotalMilliseconds;
        tempNoteData.keyA = int.Parse(line[1]);
        tempNoteData.keyB = int.Parse(line[2]);
        tempNoteData.keyC = int.Parse(line[3]);
        print(tempNoteData.timeStamp);
        noteDatas.Add(tempNoteData);

    }

    private void DialogDataProcessComplete()
    {
        this.PostNotification(ReMessageCenter.CONSOLE_ADD_LOG, "Reading Music Data Complete");
        LoadMusic();
    }

    #endregion Parse Music Data

    #region Load Music

    private void LoadMusic()
    {
        this.PostNotification(ReMessageCenter.CONSOLE_ADD_LOG, "Loading Data");
        audioImporter.Import(musicPath);
    }

    private void OnAudioLoaded(AudioClip audioClip)
    {
        this.PostNotification(ReMessageCenter.CONSOLE_ADD_LOG, "Music Loaded");
        startSongButton.interactable = true;
        openSongButton.interactable = true;
        musicSource.clip = audioClip;
        musicLenght = (audioClip.length * 1000) + 1000;
        print(musicLenght);
    }

    #endregion Load Music

    #endregion Load Song Data

    #region Game Loop

    #region MainLoop

    #region Button Methods

    private void StartGame()
    {
        if (gameRunning)
        {
            gameRunning = false;
        }
        else
        {
            if (musicSource.clip != null)
            {
                StartGamePlay();
            }
        }
    }

    private void StartGamePlay()
    {
        startSongButtonText.text = "Stop Music";
        openSongButton.interactable = false;
        ResetHit();
        gameRunning = true;
        UpdateTimeStamp();
        Timing.RunCoroutine(MainGameLoop().CancelWith(gameObject), Segment.FixedUpdate);
        //Timing.RunCoroutine(GameLoopDebug().CancelWith(gameObject), Segment.FixedUpdate);
    }

    private void StopGamePlay()
    {
        this.PostNotification(ReMessageCenter.NOTE_RESET_GAME);
        startSongButtonText.text = "Play Music";
        openSongButton.interactable = true;
        musicSource.Stop();
    }

    private void ExitGame()
    {
        Application.Quit();
    }

    #endregion Button Methods

    #region Main Game Loop

    private double gameLoopTick = 0;
    private double gameDebugLoopTick = 0;
    private int currentNote = 0;
    private double currentTimeStamp = 0;
    private bool gameRunning = false;

    private IEnumerator<float> MainGameLoop()
    {
        gameLoopTick = 0;
        currentNote = 0;
        currentTimeStamp = noteDatas[currentNote].timeStamp;
        // print("music lenght " + musicLenght);
        this.PostNotification(ReMessageCenter.CONSOLE_ADD_LOG, $"next timestamp at {currentTimeStamp} keyA {noteDatas[currentNote].keyA} keyB {noteDatas[currentNote].keyB} keyC {noteDatas[currentNote].keyC}");

        while (gameRunning)
        {
            if (gameLoopTick == currentTimeStamp)
            {
                // print("Spawn at " + gameLoopTick + " as " + currentNote);
                ShowNote(currentNote);

                //print(currentNote);
                //print(noteDatas.Count);
                if (currentNote < noteDatas.Count - 1)
                {
                    currentNote++;
                    currentTimeStamp = noteDatas[currentNote].timeStamp;
                    // print($"next timestamp at {currentTimeStamp} keyA {noteDatas[currentNote].keyA} keyB {noteDatas[currentNote].keyB} keyC {noteDatas[currentNote].keyC}");
                }
            }

            UpdateTimeStamp();

            if (!musicSource.isPlaying)
                if (gameLoopTick == 1000)
                    musicSource.Play();

            this.PostNotification(ReMessageCenter.GAME_TICK);

            yield return Timing.WaitForSeconds(.001f);
            gameLoopTick += 20;

            if (gameLoopTick >= musicLenght)
            {
                gameRunning = false;
            }

        }
        StopGamePlay();
    }

    private IEnumerator<float> GameLoopDebug()
    {
        while (gameRunning)
        {
            yield return Timing.WaitForSeconds(1f);
            gameDebugLoopTick++;
        }
    }

    private void UpdateTimeStamp()
    {
        TimeSpan tempTimeSpan = TimeSpan.FromMilliseconds(gameLoopTick);
        timeStamp.text = $"{tempTimeSpan.Minutes:00}:{tempTimeSpan.Seconds:00}:{tempTimeSpan.Milliseconds:000}";
        timeDebugStamp.text = gameLoopTick.ToString();
    }

    private void InputCheck()
    {
        if (Input.GetKeyDown(KeyCode.A))
            this.PostNotification(ReMessageCenter.NOTE_VBUTTON_PRESS_DOWN, 1);
        if (Input.GetKeyDown(KeyCode.S))
            this.PostNotification(ReMessageCenter.NOTE_VBUTTON_PRESS_DOWN, 2);
        if (Input.GetKeyDown(KeyCode.D))
            this.PostNotification(ReMessageCenter.NOTE_VBUTTON_PRESS_DOWN, 3);

        if (Input.GetKeyUp(KeyCode.A))
            this.PostNotification(ReMessageCenter.NOTE_VBUTTON_PRESS_UP, 1);
        if (Input.GetKeyUp(KeyCode.S))
            this.PostNotification(ReMessageCenter.NOTE_VBUTTON_PRESS_UP, 2);
        if (Input.GetKeyUp(KeyCode.D))
            this.PostNotification(ReMessageCenter.NOTE_VBUTTON_PRESS_UP, 3);

    }

    #endregion Main Game Loop

    #endregion MainLoop

    #region Note Processor

    #region Spawn Note

    private List<NoteData> noteDatas = new List<NoteData>();
    private List<NoteBehaviour> notePool = new List<NoteBehaviour>();
    [SerializeField] private Transform noteContainer;
    [SerializeField] private GameObject notePrefab;

    private void ShowNote(int currentNote)
    {
        if (noteDatas[currentNote].keyA == 1)
        {
            SpawnNoteObject(1, currentNote);
        }

        if (noteDatas[currentNote].keyB == 1)
        {
            SpawnNoteObject(2, currentNote);
        }

        if (noteDatas[currentNote].keyC == 1)
        {
            SpawnNoteObject(3, currentNote);
        }
    }

    private void SpawnNoteObject(int posIndex, int currentNote)
    {
        //  print("try to spawn object " + currentNote);
        NoteBehaviour currentNoteObject = GetNoteFromPool();
        currentNoteObject.SetupNote(NoteType.Normal, (NoteButtonType)posIndex);
        currentNoteObject.transform.position = spawnPoints[posIndex].position;
        currentNoteObject.name = "Note " + currentNote;
        // print("get spaw  " + currentNoteObject.name);
        currentNoteObject.gameObject.SetActive(true);
    }

    #endregion Spawn Note

    #region Note Pooler

    private void OnNoteToPool(object sender, object data)
    {
        ReturnNodeToPool((GameObject)data);
    }

    private NoteBehaviour GetNoteFromPool()
    {
        if (notePool.Count > 0)
        {
            NoteBehaviour tempObject = notePool[0];
            notePool.RemoveAt(0);
            return tempObject;
        }
        else
        {
            // print("create object");
            return Instantiate(notePrefab, noteContainer).GetComponent<NoteBehaviour>();
        }
    }

    private void ReturnNodeToPool(GameObject note)
    {
        note.SetActive(false);
        notePool.Add(note.GetComponent<NoteBehaviour>());
    }

    #endregion Note Pooler

    #endregion Note Processor

    #endregion Game Loop

}