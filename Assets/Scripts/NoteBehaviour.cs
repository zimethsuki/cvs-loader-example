using UnityEngine;

public class NoteBehaviour : MonoBehaviour
{
    #region variable

    [HideInInspector] public NoteType noteType;
    [HideInInspector] public NoteButtonType noteButtonType;
    private int zoneID = 0;
    private Vector2 tickPos;

    #endregion variable

    #region Unity Methods

    private void OnEnable()
    {
        tickPos = transform.localPosition;
        this.AddObserver(OnGameLoopTick, ReMessageCenter.GAME_TICK);
        this.AddObserver(OnResetGame, ReMessageCenter.NOTE_RESET_GAME);
        this.AddObserver(OnTriggerButtonPress, ReMessageCenter.NOTE_TRIGGERED);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGameLoopTick, ReMessageCenter.GAME_TICK);
        this.RemoveObserver(OnResetGame, ReMessageCenter.NOTE_RESET_GAME);
        this.RemoveObserver(OnTriggerButtonPress, ReMessageCenter.NOTE_TRIGGERED);
    }

    #endregion Unity Methods

    #region Responses

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("OuterZone"))
        {
            zoneID = 1;
        }
        else if (collision.CompareTag("InnerZone"))
        {
            zoneID = 2;
        }
        else
        {
            this.PostNotification(ReMessageCenter.NOTE_SCORE, 0);
            ResetNote();
        }
    }

    private void OnTriggerButtonPress(object sender, object data)
    {
        if ((NoteButtonType)data == noteButtonType)
        {
            if (zoneID == 1)
            {
                this.PostNotification(ReMessageCenter.NOTE_SCORE, 1);
                print("ONE");
                ResetNote();
            }
            else if (zoneID == 2)
            {
                this.PostNotification(ReMessageCenter.NOTE_SCORE, 2);
                ResetNote();
                print("TWO");
            }
            else
            {
                this.PostNotification(ReMessageCenter.NOTE_SCORE, 0);
                print("NONE");
            }
        }
    }

    private void OnGameLoopTick(object sender, object data)
    {
        tickPos.y -= .2f;
        transform.localPosition = tickPos;
    }

    private void OnResetGame(object sender, object data)
    {
        ResetNote();
    }

    #endregion Responses

    #region Setup

    public void SetupNote(NoteType noteType, NoteButtonType noteButtonType)
    {
        this.noteType = noteType;
        this.noteButtonType = noteButtonType;
    }

    public void ResetNote()
    {
        noteType = NoteType.None;
        zoneID = 0;
        this.PostNotification(ReMessageCenter.NOTE_TO_POOL, gameObject);
    }

    #endregion Setup

}