using TMPro;
using UnityEngine;

public class ConsoleLogBehaviour : MonoBehaviour
{
    private string logText = "";
    [SerializeField]private TMP_Text consoleWindow;

    private void OnEnable()
    {
        this.AddObserver(OnAddLog, ReMessageCenter.CONSOLE_ADD_LOG);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnAddLog, ReMessageCenter.CONSOLE_ADD_LOG);
    }

    private void OnAddLog(object sender, object data)
    {
        logText += "\n" + (string)data;
        consoleWindow.text = logText;
    }
}