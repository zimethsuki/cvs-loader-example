﻿
public class ReMessageCenter
{
    public static string GAME_TICK = "GAME_TICK";
    public static string NOTE_TRIGGERED = "NOTE_TRIGGERED";
    public static string NOTE_TO_POOL = "NOTE_TO_POOL";
    public static string NOTE_SCORE = "NOTE_SCORE";

    public static string NOTE_RESET_GAME = "NOTE_RESET_GAME";

    public static string NOTE_VBUTTON_PRESS_DOWN = "NOTE_VBUTTON_CLICK_DOWN";
    public static string NOTE_VBUTTON_PRESS_UP = "NOTE_VBUTTON_CLICK_UP";

    public static string CONSOLE_ADD_LOG = "CONSOLE_ADD_LOG";
}
