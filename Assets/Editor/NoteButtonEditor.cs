using UnityEditor;
using UnityEngine;

[InitializeOnLoadAttribute]
[CustomEditor(typeof(NoteButton))]
[CanEditMultipleObjects]
public class NoteButtonEditor : Editor
{
    private bool showButtonSetting = true;

    private SerializedProperty noteButtonType;

    private SerializedProperty interactable;
    private SerializedProperty targetGraphic;
    private SerializedProperty targetGraphicColor;
    private SerializedProperty colors;
    private SerializedProperty m_NormalColor;
    private SerializedProperty m_PressedColor;
    private SerializedProperty m_DisabledColor;
    private SerializedProperty m_FadeDuration;

    private NoteButton noteButton;

    private void OnEnable()
    {
        noteButton = serializedObject.targetObject as NoteButton;
        noteButtonType = serializedObject.FindProperty("noteButtonType");
        interactable = serializedObject.FindProperty("interactable");
        targetGraphic = serializedObject.FindProperty("targetGraphic");
        colors = serializedObject.FindProperty("colors");
        m_NormalColor = colors.FindPropertyRelative("m_NormalColor");
        m_PressedColor = colors.FindPropertyRelative("m_PressedColor");
        m_DisabledColor = colors.FindPropertyRelative("m_DisabledColor");
        m_FadeDuration = colors.FindPropertyRelative("m_FadeDuration");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(interactable);

        if (interactable.boolValue)
        {
            noteButton.targetGraphic.CrossFadeColor(m_NormalColor.colorValue, 0f, true, true);
        }
        else
        {
            noteButton.targetGraphic.CrossFadeColor(m_DisabledColor.colorValue, 0f, true, true);
        }

        EditorGUILayout.PropertyField(noteButtonType);
        showButtonSetting = EditorGUILayout.BeginFoldoutHeaderGroup(showButtonSetting, "Button Effect Setting");
        if (showButtonSetting)
        {
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(targetGraphic);
            EditorGUILayout.PropertyField(m_NormalColor);
            EditorGUILayout.PropertyField(m_PressedColor);
            EditorGUILayout.PropertyField(m_DisabledColor);
            EditorGUILayout.PropertyField(m_FadeDuration);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target,"Color graphic changed");
            }
        }

        EditorGUILayout.EndFoldoutHeaderGroup();

        serializedObject.ApplyModifiedProperties();

    }

    private void Scan()
    {
        var prop = serializedObject.GetIterator();
        do
        {
            Debug.Log(prop.name);
        } while (prop.Next(true));
    }

}